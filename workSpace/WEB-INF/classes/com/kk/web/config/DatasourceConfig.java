package com.kk.web.config;


import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;


/**
 * 
 * 참고 사이트 : http://www.mybatis.org/spring/ko/index.html
 *
 */

@Configuration
@PropertySource("classpath:config/config-${spring.profiles.active:local}.properties")
@MapperScan(basePackages={"com.kk.web.mybatis"})
public class DatasourceConfig {
	@Autowired
	ApplicationContext context;
	
	@Value("${db.dataSource.url}")
	private String mySqlUrl;
	@Value("${db.dataSource.name}")
	private String mySqlName;
	@Value("${db.dataSource.password}")
	private String mySqlPass;
	
	@Bean
	public DataSource dataSource(){
		SimpleDriverDataSource dataSource = new SimpleDriverDataSource();
		dataSource.setDriverClass(org.mariadb.jdbc.Driver.class);
		dataSource.setUrl("jdbc:mariadb://182.162.90.17/KKDB");
		dataSource.setUsername("root");
		dataSource.setPassword("q1w2e3r4!");
		
		return dataSource;
	}
	
    @Bean
    public SqlSessionFactory sessionFactory() throws Exception {
        SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
        sessionFactory.setDataSource(dataSource());
        //sessionFactory.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath:mabatis/mapper/*.xml"));
        //sessionFactory.setConfigLocation(new ClassPathResource("mybatis/mybatis-config.xml"));
        sessionFactory.setConfigLocation(context.getResource("classpath:mybaits/mybatis-config.xml"));
        sessionFactory.setMapperLocations(context.getResources("classpath:mybaits/mapper/*.xml"));
        return sessionFactory.getObject();
    }
    
    @Bean
    public SqlSessionTemplate sqlSessionTemplate() throws Exception {
        return new SqlSessionTemplate(sessionFactory());
    }

    
    
	
	
	/* mapper 등록 >>  @MapperScan 대체
	 * @Bean
    public UserMapper userMapper() throws Exception {
      SqlSessionTemplate sessionTemplate = new SqlSessionTemplate(sqlSessionFactory());
      return sessionTemplate.getMapper(UserMapper.class);
    }*/
}
