package com.kk.web.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;


/**
 * Configuration 은 이 빈이 스프링 설정과 관련되어 있음을 알려준다. 
 * ComponentScan 은 해당 패키지를 기준으로 Annotation을 읽어 들어오겠단 것입니다. 
 * EnableWebMvc는 자바소스기반으로 스프링 MVC를 설정함을 나타낸다.(mvc:annotation-driven)
 * 
 * 클래스 내용을 보면 일반적인 XML 기반의 설정과 같다고 보면 될거 같다. 
 * 지금은 여기서는 단순히 ViewResolver 만 설정해 주었지만, 
 * 기존 XML에서 injection을 받는 부분은 Autowired 어노테이션을 이용하여 가지고 와서 각 Bean에 셋팅해주는 작업들을 진행할 것이다.
 * 
 * 
 	<context:component-scan base-package= "com.bookstore.controllers" />
    <bean
        class="org.springframework.web.servlet.view.InternalResourceViewResolver" >
        <property name="prefix" value= "/WEB-INF/views/" />
        <property name="suffix" value= ".jsp" />
        <property name="order" value= "1" />
    </bean >
    <mvc:annotation-driven />
*/

@Configuration
@PropertySource("classpath:config/config-${spring.profiles.active:local}.properties")
@EnableWebMvc
@ComponentScan(basePackages = { "com.kk.web" })
public class ServerConfig extends WebMvcConfigurerAdapter {
	
	/**
     * 인터셉터 (요청을 가로챔)
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
    	System.out.println("22addInterceptors()");
        LocaleChangeInterceptor localeChangeInterceptor = new LocaleChangeInterceptor();
        localeChangeInterceptor.setParamName("lang");
        registry.addInterceptor(localeChangeInterceptor);
 
    }
    
    /**
     * locale resolver
     *
     * @return
     */
    @Bean
    public LocaleResolver localeResolver() {
    	System.out.println("3333localeResolver()");
        CookieLocaleResolver cookieLocaleResolver = new CookieLocaleResolver();
        cookieLocaleResolver.setDefaultLocale(StringUtils.parseLocaleString("en"));
        return cookieLocaleResolver;
    }
    
    @Autowired
    ResourceLoader resourceLoader;
    
	/**
     * JSP등을 뷰로 사용하는 ViewResolver 등록
     */
	@Bean
	public InternalResourceViewResolver viewResolver() {
		System.out.println("4444InternalResourceViewResolver()");
		InternalResourceViewResolver  resolver = new InternalResourceViewResolver();
		resolver.setPrefix("/WEB-INF/views/");
		resolver.setSuffix(".jsp");
		resolver.setViewClass(JstlView.class);	
		resolver.setContentType( "text/html; cherset=UTF-8"); // 기본적인 인코딩 관련 정보
		return resolver;
	}
	
	
	/**
     * CSS / JavaScript / Image 등의 정적 리소스를 처리해주는 핸들러를 등록
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
    	System.out.println("555555555addResourceHandlers()");
        registry.addResourceHandler("/static/**").addResourceLocations("/static/");
    }
      
	
} 