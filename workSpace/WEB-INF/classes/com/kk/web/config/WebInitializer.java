package com.kk.web.config;

import javax.servlet.Filter;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import com.kk.web.filter.XSSFilter;

/**
 * WebInitializer.java 는 web.xml에서 설정하던 Servlet 관련 설정을 대신 한다. 서블릿에 Annotation 기반
 * 환경설정을 할 ServerConfig.class를 설정한다.
 */

public class WebInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {
	@Override
	protected Class<?>[] getServletConfigClasses() {
		// 서블릿을 설정한 config 클래스 jsp 설정 해논 클래스
		System.out.println("111WebInitializer.getServletConfigClasses()");
		return new Class<?>[] { ServerConfig.class };
	}
	
	@Override
	protected Class<?>[] getRootConfigClasses() {
		// 기본 설정들 config 클래스 넣어야함
		return new Class<?>[] { DatasourceConfig.class };
		//return null;
	}


	@Override
	protected String[] getServletMappings() {
		return new String[] { "/" };
	}

	@Override
	protected Filter[] getServletFilters() {
		CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
		characterEncodingFilter.setEncoding("UTF-8");
		characterEncodingFilter.setForceEncoding(Boolean.TRUE);
		
		XSSFilter crossSiteScriptFiter = new XSSFilter();
		
		return new Filter[] { characterEncodingFilter , crossSiteScriptFiter};
	}

}