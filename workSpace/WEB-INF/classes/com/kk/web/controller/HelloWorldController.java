package com.kk.web.controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.kk.web.domain.Board;
import com.kk.web.mybatis.Mapper;

@Controller
public class HelloWorldController {

	@RequestMapping("/{id}")
	public String hello(@PathVariable String id, Model model) {
		model.addAttribute("name", id);
		return "/index";
	}

	@RequestMapping("/{id}/index.do")
	public String index(@PathVariable String id, Model model) {
		model.addAttribute("name", id);
		return "/index";
	}

	@RequestMapping("/subPage/sample.do")
	public String sample(Model model) {
		model.addAttribute("name", "aaa");
		return "/subPage/sample";
	}
	
	@Autowired
    private Mapper mapper;
	
	@RequestMapping("/subPage/mybatisTest.do")
	public String dbTest(HttpServletRequest req, HttpServletResponse res , Model model, Board board,  @RequestParam("name") String name) {
		//param : String , result : String
		String title = mapper.selectBoardString(name);
		model.addAttribute("title", title);
		
		//param : model , result : model
		board.setRegName(name);
		Board boardInfo = mapper.selectBoardDomain(board);
		model.addAttribute("boardInfo", boardInfo);
		
		//param : map , result : map
		Map<Object, String> map = new HashMap<Object, String>();
		map.put("regName", name);
		map = mapper.selectBoardMap(map);
		model.addAttribute("map", map);
		
        return "/subPage/mybatisTest";
	}
}
