package com.kk.web.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kk.web.domain.Board;
import com.kk.web.domain.User;
import com.kk.web.mybatis.Mapper;

/**
 * 
 * RestController 라는 어노테이션이 보이는데, 기존에 Controller + ResponseBody 를 대신해준다.
 * 화면 리턴이 없이 값만 리턴 하는 경우(데이터 전송)등에 사용 한다.
 * 
 */

@RestController
public class RestServiceController {
	@RequestMapping(value="/hello/{id}/{name}", method=RequestMethod.GET)
	public User hello( @PathVariable String name,
			           @PathVariable String id,
			           Model model) {
		System.out.println("#1 : " + name );
		System.out.println("#2 : " + id );
		User newUser = new User(id,name); 
		return newUser;
	}
}
