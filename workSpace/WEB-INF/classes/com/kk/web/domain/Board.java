package com.kk.web.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class Board {
	private String no;
	private String title;
	private String content;
	private String hit;
	private String regName;
	private String regDate;
	private String updateName;
	private String updateDate;
	private String deleteDate;
	private String password;
	private String fileNo;
}

