package com.kk.web.filter;

import java.io.IOException;
import java.util.regex.Pattern;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

public class XSSFilter implements Filter {
	public void init(FilterConfig arg0) throws ServletException {
	}

	public void destroy() {
	}

	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {
		chain.doFilter(new XSSRequestWrapper((HttpServletRequest) req), res);
	}

	public class XSSRequestWrapper extends HttpServletRequestWrapper {
		private Pattern[] patterns = new Pattern[] {
			Pattern.compile("<script>(.*?)</script>", Pattern.CASE_INSENSITIVE),
			Pattern.compile("src[\r\n]*=[\r\n]*\\\'(.*?)\\\'",Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
			Pattern.compile("src[\r\n]*=[\r\n]*\\\"(.*?)\\\"",Pattern.CASE_INSENSITIVE | Pattern.MULTILINE| Pattern.DOTALL),
			Pattern.compile("</script>", Pattern.CASE_INSENSITIVE),
			Pattern.compile("<script(.*?)>", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
			Pattern.compile("eval\\((.*?)\\)", Pattern.CASE_INSENSITIVE	| Pattern.MULTILINE | Pattern.DOTALL),
			Pattern.compile("expression\\((.*?)\\)", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
			Pattern.compile("javascript:", Pattern.CASE_INSENSITIVE),
			Pattern.compile("vbscript:", Pattern.CASE_INSENSITIVE),
			Pattern.compile("onload(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL)
		};

		public XSSRequestWrapper(HttpServletRequest request) {
			super(request);
		}

		public String[] getParameterValues(String parameter) {
			String[] values = super.getParameterValues(parameter);
			if (values == null) {
				return null;
			}
			int count = values.length;
			String[] encodedValues = new String[count];
			for (int i = 0; i < count; i++) {
				encodedValues[i] = stripXSS(values[i]);
			}
			return encodedValues;
		}

		public String getParameter(String parameter) {
			return stripXSS(super.getParameter(parameter));
		}

		public String getHeader(String name) {
			return stripXSS(super.getHeader(name));
		}

		private String stripXSS(String value) {
			if (value != null) {
				// ESAPI library 이용하여 XSS 필터를 적용하려면 아래 코드의 커맨트를 제거하고 사용한다. 강력추천!!
				// value = ESAPI.encoder().canonicalize(value);
				// null 문자를 제거한다.
				value = value.replaceAll("\0", "");
				// 패턴을 포함하는 입력에 대해 <, > 을 인코딩한다.
				for (Pattern scriptPattern : patterns) {
					if(scriptPattern.matcher(value).find()) {
						value = value.replaceAll("<", "&lt;").replaceAll(">", "&gt;");
					}
				}
			}
			return value;
		}
	}
}
