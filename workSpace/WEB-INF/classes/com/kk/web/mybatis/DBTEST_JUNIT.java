package com.kk.web.mybatis;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import org.junit.Test;

public class DBTEST_JUNIT {
	private String title = "";
	private String content = "";
	private String driver = "com.mysql.jdbc.Driver";
	private String url = "jdbc:mariadb://182.162.90.17:3306/KKDB";
	private String uid = "root";
	private String upw = "q1w2e3r4!";
	private String query = "SELECT * FROM BOARD WHERE REG_NAME='수'";
	
	/*@Test
    public void test() {
        
        try {
            Class.forName(driver);
            
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try (Connection con = DriverManager.getConnection(url, uid, upw)){
        	 System.out.println(111);
        	System.out.println(con);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/
	
	
    @Test
    public void test() {
    	Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;

		try {
			Class.forName(driver);
			connection = (Connection) DriverManager.getConnection(url, uid, upw);
			statement = (Statement) connection.createStatement();
			resultSet = statement.executeQuery(query);
			while (resultSet.next()) {
				title = resultSet.getString("title");
				content = resultSet.getString("content");
				System.out.println("title : " + title + ", content : " + content);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally { // 모든 자원 해제
			try {
				if (resultSet != null) {
					resultSet.close();
				}
				if (statement != null) {
					statement.close();
				}
				if (connection != null) {
					connection.close();
				}
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
    }
}
