package com.kk.web.mybatis;

import java.util.List;
import java.util.Map;

import org.springframework.transaction.annotation.Transactional;

import com.kk.web.domain.Board;

@Transactional
public interface Mapper {
	
	public String selectBoardString(String name);
	
	public Board selectBoardDomain(Board board);
	
	@SuppressWarnings("rawtypes")
	public Map<Object, String> selectBoardMap(Map map);
	
	@SuppressWarnings("rawtypes")
	public List<Map<Object, String>> selectBoardList(Map map);
	
}

