<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="format-detection" content="telephone=no">
<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, width=device-width, target-densitydpi=medium-dpi">

<link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/static/css/style.css">



<script type="text/javascript" src="<%=request.getContextPath() %>/static/js/jquery/jquery-2.2.4.min.js" ></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/static/js/jquery/jquery-ui.js" ></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/static/js/jquery/swiper.min.js" ></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/static/js/common.js" ></script>
