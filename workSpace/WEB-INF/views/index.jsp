<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page session="false"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@include file="./include/header.jsp" %>
	<title>KKProject_WEB</title>
</head>


<body>
	<h1>Hello :</h1>
	<c:out value="Hello JSTL"></c:out>
	<c:out value="${name}"></c:out>
</body>


<script type="text/javascript">
	$(document).ready(function(){
		alert("${name}");	
	});
</script>


</html>
