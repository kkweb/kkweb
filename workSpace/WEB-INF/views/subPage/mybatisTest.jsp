<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page session="false"%>

<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
</head>
<body>
<h1>mybatisTest</h1>


<h1>string >>> <c:out value="${title}" /></h1>

===============================================

<h1>model >>> <c:out value="${boardInfo.getTitle()}" /></h1>

===============================================

<h1>map11 >>><c:out value="${map.TITLE}" /></h1>
<h1>map1 >>><c:out value="${map.REG_NAME}" /></h1>
</body>

</html>