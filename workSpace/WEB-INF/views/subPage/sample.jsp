<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page session="false"%>

<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

<link rel="stylesheet" href="/static/css/webReset.css" />
<link rel="stylesheet" href="/static/css/webMain.css" />
<!--[if lt IE 9]><link rel="stylesheet" href="/static/common/css/pcweb/webContents_ie8.css"/> <![endif]-->
<style type="text/css">
.popup_wrap {
	position: absolute;
	top: 60px;
	left: 60px;
	z-index: 10000
}

.popup_in {
	border: solid 1px #305d94;
	position: relative;
}

.popup_close {
	position: absolute;
	right: -70px;
	top: -1px;
}

.popup_check {
	height: 60px;
	text-align: right;
	background: #fff;
	padding-right: 20px;
}

.popup_check span {
	padding-top: 20px;
	display: block;
	font-size: 20px;
	color: #305d94;
}

#pcWebNotice2 .popup_in {
	border: 1px solid #5735a5;
}

#pcWebNotice2 .popup_check span {
	color: #28184b;
}

.popup_wrap2 {
	position: absolute;
	top: 150px;
	left: 60px;
	z-index: 10000
}

.popup_in2 {
	border: solid 1px #888888;
	position: relative;
}

.popup_close2 {
	position: absolute;
	right: 6px;
	top: 535px;
}

.popup_check2 {
	position: absolute;
	top: 537px;
	text-align: left;
	padding-left: 10px;
}

.popup_check2 span {
	display: block;
	font-size: 14px;
	color: #666;
}

.popup_check2 span img {
	padding-top: 2px;
}

#pcWebNotice2 .popup_in2 {
	border: solid 1px #5735a5;
}

#pcWebNotice2 .popup_check2 span {
	color: #28184b;
}

.MDmail {
	width: 283px;
	height: 73px;
	padding: 20px;
	box-shadow: 0px 0px 15px #ddd;
	background-color: #fff;
	border: none;
	text-align: center;
}

.MDmail input {
	width: 283px;
	height: 32px;
	border: 1px solid #ddd;
	box-sizing: border-box;
	text-align: center;
}

.MDmail div {
	padding-top: 10px;
}

.MDmail div a.copy {
	display: inline-block;
	width: 138px;
	margin-right: 7px;
	line-height: 32px;
	color: #fff;
	border-radius: 5px;
	background-color: #e53738;
	text-decoration: none;
}

.MDmail div a.cancel {
	display: inline-block;
	width: 138px;
	line-height: 32px;
	color: #fff;
	border-radius: 5px;
	background-color: #afafaf;
	text-decoration: none;
}

.MDmail div a:hover {
	font-weight: bold;
}
</style>
<title>SK stoa</title>
</head>
<body>
	<div class="skip_nav">
		<a href="#container">본문 바로가기</a>
	</div>
	<script type="text/javascript">
		var api_server_uri = 'http://api.bshopping.co.kr/front';
		var static_path = 'http://m.skstoa.com';
		var payment_path = 'https://m.skstoa.com';
	</script>
	<header>
		<div class="inner posR">
			<h1 class="logo">
				<a href="/pcweb/index"><img
					src="http://image.bshopping.co.kr/webMain/main/logo.png"
					width="118" height="55" alt="SKSTOA" /></a>
			</h1>
			<div class="contact_box">
				<span><a href="/pcweb/contactUs">CONTACT US</a></span> <span><a
					href="/pcweb/siteMap">SITE MAP</a></span>
			</div>
		</div>
		<div class="gnb">
			<ul class="inner">
				<li><a href="javascript:;">회사소개</a></li>
				<li><a href="javascript:;">사업소개</a></li>
				<li><a href="javascript:;">홍보센터</a></li>
				<li><a href="javascript:;">입점 및 제휴</a></li>
				<li><a href="javascript:;">고객센터</a></li>
				<!-- <li><a href="javascript:;">편성표</a></li> -->
			</ul>
		</div>
		<div class="snb" style="display: none;">
			<div class="inner">
				<ul class="snb_01">
					<li><a href="/pcweb/intro">회사개요</a></li>
					<li><a href="/pcweb/history">연혁</a></li>
					<li><a href="/pcweb/greeting">CEO 인사말</a></li>
					<li><a href="/pcweb/manageEthics">윤리경영</a></li>
					<!--<li><a href="/pcweb/recruit">채용안내</a></li>-->
				</ul>
				<ul class="snb_02">
					<li><a href="/pcweb/tvShopping">T 커머스</a></li>
					<!--<li><a href="/pcweb/advertisement">광고사업</a></li>-->
				</ul>
				<ul class="snb_03">
					<li><a href="/pcweb/news">보도자료</a></li>
					<!--<li><a href="/pcweb/broadcastingAd">방송광고</a></li>-->
					<li><a href="/pcweb/ci">CI 소개</a></li>
					<!--<li><a href="/pcweb/socialCont">사회공헌</a></li>-->
				</ul>
				<ul class="snb_04">
					<li><a href="/pcweb/launching">입점/상품 제안</a></li>
					<li><a href="/pcweb/marketing">제휴마케팅 제안</a></li>
					<!--<li><a href="/pcweb/adContact">광고문의</a></li>-->
				</ul>
				<ul class="snb_05">
					<li><a href="/pcweb/notice">공지사항</a></li>
					<li><a href="/pcweb/faq">FAQ</a></li>
					<li><a href="/pcweb/commission">시청자위원회</a></li>
				</ul>
				<!-- <div class="video"><img src="http://image.bshopping.co.kr/webMain/main/img_video01.jpg" width="226" height="127" alt="" /></div> -->
			</div>
		</div>
	</header>
	<div class="container">
		<div class="visualWrap">
			<div id="slideBanner1" class="ui-slide-banner">
				<ul class="ui-slide-banner-list">
					<li class="ui-slide-banner-list-item"><img
						src="http://image.bshopping.co.kr/webMain/mvisualimg01.jpg" alt="" />
						<!--<div class="visualText">
							<p class="title">행복한 쇼핑 SK스토아!</p>
							<p class="text">
								SK stoa는 중소기업의 알짜배기 상품들을 소개해드려, 고객님들의 행복한 쇼핑생활을 책임지는<br />
								SK스토아가 만든 데이터 쇼핑 서비스입니다. TV와 모바일로 만나보실 수 있습니다.
							</p>
						</div>--></li>

					<li class="ui-slide-banner-list-item"><img
						src="http://image.bshopping.co.kr/webMain/mvisualimg02.jpg" alt="" />
						<!--<div class="visualText">
							<p class="title">행복한 쇼핑 SK스토아!</p>
							<p class="text">
								SK 스토아는 중소기업의 알짜배기 상품들을 소개드려, 고객님들의 행복한 쇼핑생활을 책임지는<br>
								SK스토아가 만든 데이터 쇼핑 서비스 입니다. TV와 모바일로 만나보실 수 있습니다.
							</p>
						</div>--></li>
				</ul>

				<div class="ui-slide-btn-group">
					<button class="ui-slide-prev-btn">이전</button>
					<button class="ui-slide-next-btn">다음</button>
				</div>
			</div>

			<div class="whiteBoxWrap">
				<div>
					<div class="whiteBox">
						<div onclick="location.href='/pcweb/mobileShopping';">
							<h2 class="">모바일쇼핑</h2>
							<p>
								SK stoa TV 방송 상품을 포함한 다양한 상품들을<br />모바일 WEB과 APP에서 한 눈에 확인하세요.
							</p>
						</div>
						<span class="whiteBg"
							onclick="location.href='/pcweb/mobileShopping';">BG</span>
						<ul class="barcode_box">
							<li><img
								src="http://image.bshopping.co.kr/webMain/main/barcode01.png"
								alt="모바일바코드" /></li>
							<li><img
								src="http://image.bshopping.co.kr/webMain/main/barcode02.png"
								alt="IOS바코드" /></li>
							<li><img
								src="http://image.bshopping.co.kr/webMain/main/barcode03.png"
								alt="안드로이드바코드" /></li>
						</ul>
					</div>
				</div>
				<div>
					<div class="whiteBox">
						<div onclick="location.href='/pcweb/tvShopping';">
							<h2 class="">TV쇼핑</h2>
							<p>
								영상으로 보는 생생한 상품정보!<br />실시간으로 받아보는 행복쇼핑 팁!
							</p>
						</div>
						<span class="whiteBg" onclick="location.href='/pcweb/tvShopping';">BG</span>
					</div>
				</div>
			</div>
		</div>
		<div class="contents inner">
			<ul>
				<li class="contents01"><img
					src="http://image.bshopping.co.kr/webMain/main/Btv360vr_re.jpg"
					width="368" height="392" alt="Btv360VR" />
					<div class="contents_inner">
						<div class="list_title_box">
							<h3 class="list_title">홍보센터</h3>
							<span class="the_see"><a href="/pcweb/news">더보기</a></span>
						</div>
						<div class="title">
							<a href="/pcweb/news/detail?boardNo=457">SK스토아, 실속형 설 명절 특집전
								진행</a>
						</div>
						<div class="text">
							<a href="/pcweb/news/detail?boardNo=457"><p>
									<span style="font-size: 16px"><strong>SK스토아 실속형
											설 명절 특집전 진행</strong></span>
								</p>

								<p>&nbsp;</p>

								<p>
									<span style="font-size: 16px">&middot;<strong>&nbsp;명절맞이
											신선식품 2일 배송 보장</strong><br /> &middot;<strong>&nbsp;모바일 앱에
											3만원 이하 실속 선물세트 구성</strong></span>
								</p>

								<div class="term_sec">
									<div class="box">
										<p>T커머스 업체인 SK스토아 (대표 윤석암 www.skstoa.com)가 2월 설 명절 기간을 맞이해
											&lsquo;2018&nbsp;설 선물관&rsquo; 특집전을 진행한다고 5일 밝혔다.</p>

										<p>먼저 명절을 한주 앞둔 5일 부터는 방송을 통해 막바지 신선 식품 제품 위주의 다양한
											&lsquo;설맞이 명품 선물&rsquo; 맞춤형 단독 상품 방송을 편성할 예정이다.</p>

										<p>먼저 &lsquo;배연정 안창살 구이 세트(420g 9팩)&rsquo; 59,900원,
											&lsquo;소들녁 돼지왕구이세트(총 3.41kg)&rsquo; 54,900원, &lsquo;고래사 어묵어전
											세트(4종, 각2봉, 총12봉)&rsquo; 49,900원에</p>

										<p>구매가능하다.&nbsp; 또한 &lsquo;사임당 콩 쑥개떡 세트(4kg,
											총100개)&rsquo;를 40,900원에구매할 수 있다. 이외에도 명절 건강식품 선물로 인기 있는 CJ
											제일제당의</p>

										<p>&lsquo;한뿌리 홍삼기력(6박스, 총 180포)&rsquo; 198,000원, 정관장의
											&lsquo;홍삼정 마일드 센스(6박스, 총 180포)&rsquo; 365,000원,
											&lsquo;홍삼정옥고(각 100g, 9병)&rsquo; 198,000원에 선보인다.</p>

										<p>&nbsp;</p>

										<p>모바일 앱 에서 주문시 매일 3,000원의 적립금을 즉시 사용할 수 있다. 또한 금주의 추천 상품
											구성을 통해 한우축산, 과일특산, 생선수산, 홍삼, 명품 꿀 등을</p>

										<p>최대 71% 저렴하게 구매할 수 있다. 특히 신선식품 경주축협의 한우축산과 문경 APC의 사과세트의
											경우 2일안에 배송 받을 수 있다. 이와 함께 품격은 높이고 가격은</p>

										<p>낮추는 3만원 이하 실속 선물세트로 과일, 가공식품, 한과, 곶감, 차, 꿀, 소금세트, 양말세트
											등 300종의 다양한 단독 특가상품들이 함께 판매될 예정이다.</p>

										<p>&nbsp;</p>

										<p>이외에도 특히 모바일 앱에 행복나래를 통한 생필품 위주의 사회적 기업제품을 편성했다. 현재 격월로
											방송편성을 진행하고 있으나, 향후 기업발굴을 통한 상품군도 확대</p>

										<p>할 예정이며, 주기적인 방송편성도 추가로 진행해 나갈 계획이다. 이는 그룹차원에서 진행되는 사회적
											가치 창출을 일환으로 SK스토아에서는 &lsquo;착한소비&rsquo; 지향할 수 있는</p>

										<p>사회적 분위기 조성 뿐 아니라 파트너와 건강한 동반성장 환경조성을 통한 시너지를 마련하고자 하는
											일환이기도 하다.</p>

										<p>&nbsp;김판수 SK스토아 본부장은 &ldquo;SK 스토아로 출발해서 처음 맞이하는 설 명절
											특집전을 진행하는 것으로 엄격한 심사를 통해 준비하는 만큼 알찬 단독구성으로 제안하는</p>

										<p>선물세트로 편성했다. 또한 신선식품 한우, 사과세트의 경우 2일안에 배송받을 수 있을 뿐 아니라,
											3만원 미만대 선물세트의 경우 선물을 준비하는 고객이 모두</p>

										<p>만족할 수 있는 의미 있는 선물 세트&rdquo;라고 말했다.</p>

										<p>
											<img alt=""
												src="//image.bshopping.co.kr/htmleditor/299/02031518069695279.png"
												style="height: 391px; width: 800px" />
										</p>

										<p>&nbsp;</p>

										<p>
											☞보충 취재 및 문의<br /> SK스토아 기획/심의팀 유지원 매니저 (☎ 02-6266-5590)
										</p>
									</div>
								</div> </a>
						</div>
						<div class="day">2018.02.05</div>
					</div></li>
				<li class="contents02"><img
					src="http://image.bshopping.co.kr/webMain/main/shoppingtoAll_re.jpg"
					alt="SKstoa" /></li>
				<li class="contents03">
					<div class="contents_inner">
						<div class="list_title_box">
							<h3 class="list_title">공지사항</h3>
							<span class="the_see"><a href="/pcweb/notice">더보기</a></span>
						</div>
						<div class="title">
							<a href="/pcweb/notice/detail?boardNo=484">[공지] 제1기 결산공고</a>
						</div>
						<div class="day">2018.03.28</div>
						<table class="board_list">
							<caption>메인 공지사항 리스트</caption>
							<colgroup>
								<col style="width: 70%;">
								<col style="width: auto;">
							</colgroup>
							<tbody>
								<tr>
									<td class="board_text"><a
										href="/pcweb/notice/detail?boardNo=445">[공지]SK스토아 GRAND
											OPEN 안내 </a></td>
									<td class="board_date">2018.01.01</td>
								</tr>
								<tr>
									<td class="board_text"><a
										href="/pcweb/notice/detail?boardNo=416">[공지] Lifestyle
											Curation Digital Platform SK스토아 개국 안내 </a></td>
									<td class="board_date">2017.12.01</td>
								</tr>
								<tr>
									<td class="board_text"><a
										href="/pcweb/notice/detail?boardNo=415">[공지] 개인정보 처리방침 변경
											안내 </a></td>
									<td class="board_date">2017.12.01</td>
								</tr>
								<tr>
									<td class="board_text"><a
										href="/pcweb/notice/detail?boardNo=414">[공지] 이용약관 변경 안내 </a></td>
									<td class="board_date">2017.12.01</td>
								</tr>
							</tbody>
						</table>
					</div>
				</li>
				<li class="contents04">
					<div class="contents_inner">
						<div class="list_title_box">
							<h3 class="list_title">입점상품/제휴마케팅 제안</h3>
							<span class="the_see"><a href="/pcweb/launching">더보기</a></span>
						</div>
						<ul class="list_icon">
							<li class="bdb1 list_icon01"><a href="javascript:;"
								onclick="doCopy('female_fashion@skstoa.com','여성의류 입점문의');">여성의류</a></li>
							<li class="bdb1 list_icon02"><a href="javascript:;"
								onclick="doCopy('shoe_bag@skstoa.com','패션잡화 입점문의');">패션잡화</a></li>
							<li class="bdb1 list_icon03"><a href="javascript:;"
								onclick="doCopy('male_fashion@skstoa.com','남성의류/속옷 입점문의');">남성의류/속옷</a></li>
							<li class="bdb1 list_icon04"><a href="javascript:;"
								onclick="doCopy('home_fashion@skstoa.com','홈패션 입점문의');">홈패션</a></li>
							<li class="bdb1 list_icon05"><a href="javascript:;"
								onclick="doCopy('beauty@skstoa.com','이미용 입점문의');">이미용</a></li>
							<li class="bdb1 list_icon06"><a href="javascript:;"
								onclick="doCopy('furniture@skstoa.com','가구/리빙 입점문의');">가구/리빙</a></li>
							<li class="bdb1 bdr0 list_icon07"><a href="javascript:;"
								onclick="doCopy('elect_home@skstoa.com','대형가전 입점문의');">대형가전</a></li>
							<li class="list_icon08"><a href="javascript:;"
								onclick="doCopy('elect_living@skstoa.com','생활가전 입점문의');">생활가전</a></li>
							<li class="list_icon09"><a href="javascript:;"
								onclick="doCopy('service@skstoa.com','여행/서비스 입점문의');">여행/서비스</a></li>
							<li class="list_icon10"><a href="javascript:;"
								onclick="doCopy('living@skstoa.com','생활용품 입점문의');">생활용품</a></li>
							<li class="list_icon11"><a href="javascript:;"
								onclick="doCopy('elect_kitchen@skstoa.com','주방가전 입점문의');">주방가전</a></li>
							<li class="list_icon12"><a href="javascript:;"
								onclick="doCopy('food@skstoa.com','식품 입점문의');">식품</a></li>
							<li class="list_icon13"><a href="javascript:;"
								onclick="doCopy('leisure@skstoa.com','스포츠/레저 입점문의');">스포츠/레저</a></li>
							<li class="bdr0 list_icon14"><a href="javascript:;"
								onclick="doCopy('kitchen@skstoa.com','주방용품 입점문의');">주방용품</a></li>
						</ul>
					</div>
				</li>
			</ul>
		</div>

		<!--<div class="video02">
			<img src="http://image.bshopping.co.kr/webMain/main/img_video02.jpg" alt="" />
			<div class="visualText">
				<p class="title">행복한 쇼핑 SK 스토아와 함께하세요!</p>
				<p class="text">
					TV와 모바일로 만나는 행복한 쇼핑생활. SK 스토아가 책임지겠습니다.
				</p>
			</div>
			<div class="playBox">
				<button type="button" href="#">재생버튼</button>
			</div>
		</div>-->

		<div class="suggestion">
			<ul>
				<li class="faq"><a href="/pcweb/faq">FAQ</a></li>
				<li class="suggestion01"><a href="/pcweb/marketing">제휴마케팅
						제안</a></li>
				<li class="suggestion02"><a href="/pcweb/launching">입점/상품
						제안</a></li>
			</ul>
		</div>
	</div>
	<div class="footer">
		<div class="inner">
			<h1>
				<a href="#"><img
					src="http://image.bshopping.co.kr/webMain/main/foot_logo.png"
					width="118" height="55" alt="푸터로고" /></a>
			</h1>
			<div class="text_box">
				<p class="text01">
					<span><a href="/pcweb/terms">이용약관</a></span> <span><a
						href="/pcweb/policy">개인정보처리방침</a></span> <span><a
						href="http://www.ftc.go.kr/bizCommPop.do?wrkr_no=&apv_perm_no=2017313020130202290"
						target="_blank">사업자 정보 확인</a></span>
				</p>
				<p class="text02">
					에스케이스토아주식회사 / 대표이사 : 윤석암 / 대표번호 : 1566-0106 / 주소 : 서울특별시 마포구 월드컵북로
					402 (상암동 1601, KGIT 센터)<br> 사업자등록번호 : 492-87-00855 / 통신판매업신고번호
					: 2017-서울마포-2280 / 개인정보관리책임자 : 김판수<br> 고객센터 : 평일 오전 9시 ~ 오후 6시
					/ ☏ 1566-0106 / <a href="mailto:customercscenter@skstoa.com">customercscenter@skstoa.com</a>
				</p>
				<address>ⓒ 2017 by SK stoa Co., Ltd. All Rights Reserved.
				</address>
			</div>
		</div>
	</div>
	<div class="popup_wrap2" id="pcWebNotice4" style="display: none;">
		<div class="popup_in2">
			<a href="#" class="popup_close2"><img
				src="http://image.bshopping.co.kr/htmleditor/870/171227_popup_btn_close1514357418399.png"
				alt="닫기"></a>
			<div class="popup_img2">
				<a
					href="/pcweb/notice/detail?boardNo=445&currentPage=1&searchGb=&searchKey="><img
					src="http://image.bshopping.co.kr/htmleditor/226/171227_popup1514357405001.jpg"
					alt=""></a>
			</div>
			<div class="popup_check2">
				<span><label><input type="checkbox"><img
						src="http://image.bshopping.co.kr/htmleditor/838/171227_popup_txt_close1514357428862.png"
						alt=""></label></span>
			</div>
		</div>
	</div>
	<!-- //2017-09-18 소비자위원 모집 팝업 -->
	<script type="text/javascript"
		src="/static/common/js/jquery-2.1.4.min.js"></script>
	<script type="text/javascript"
		src="/static/common/js/jquery.easing.1.3.js"></script>
	<script type="text/javascript" src="/static/common/js/TweenMax.min.js"></script>
	<script type="text/javascript" src="/static/common/js/jquery-web.ui.js"></script>
	<script type="text/javascript"
		src="/static/common/js/jquery.alsEN-1.0.min.js"></script>
	<!--<script type="text/javascript" src="../resources/js/lib/swiper.min.js"></script>-->
	<script type="text/javascript"
		src="/static/common/js/publish/pc_design.js"></script>
	<script type="text/javascript">
		jQuery(window).ready(function() {
			//makeCookiePopup( jQuery( "#pcWebNotice4" ) );
		});

		/*cookie popup 2016.05.20 수정*/
		function makeCookiePopup(target) {
			var cookieName = jQuery(target).attr("id");

			var cookiePop = {
				init : function() {
					this.closeBtn = jQuery(target).find(".popup_close2"); //jQuery("#appNotice1 .m_close_btn");
					this.todayBtn = jQuery(target).find(".popup_check2"); //jQuery("#appNotice1 .m_today_btn");
					//target.after("<div class='m_dim'></div>");
					target.show();
					this.addEvent();
					var self = this;
				},

				setCookie : function(name, value, expiredays) {
					var todayDate = new Date();
					todayDate.setDate(todayDate.getDate()
							+ parseInt(expiredays));
					document.cookie = name + "=" + escape(value) + "; expires="
							+ todayDate.toGMTString() + ";";
				},

				getCookie : function(name) {
					var nameOfCookie = name + "=";
					var x = 0;
					while (x <= document.cookie.length) {
						var y = (x + nameOfCookie.length);
						if (document.cookie.substring(x, y) == nameOfCookie) {
							if ((endOfCookie = document.cookie.indexOf(";", y)) == -1)
								endOfCookie = document.cookie.length;
							return unescape(document.cookie.substring(y,
									endOfCookie));
						}
						x = document.cookie.indexOf(" ", x) + 1;
						if (x == 0)
							break;

					}
					return "";
				},

				addEvent : function() {
					var self = this;
					this.closeBtn.on("click", function() {
						self.close();
					});
					this.todayBtn.on("click", function() {
						//self.setCookie("appNotice1","done",1);
						self.setCookie(cookieName, "done", 1);
						//self.close();
					});
				},

				close : function() {
					//var target = jQuery("#appNotice1");
					var self = this;
					target.hide();
					return false;

				}
			}

			if (cookiePop.getCookie(cookieName) != "done") {
				jQuery(target).length && cookiePop.init();
			}
		}
		function is_ie() {
			if (navigator.userAgent.toLowerCase().indexOf("chrome") != -1) {
				return false;
			}
			if (navigator.userAgent.toLowerCase().indexOf("msie") != -1) {
				return true;
			}
			if (navigator.userAgent.toLowerCase().indexOf("windows nt") != -1) {
				return true;
			}
			return false;
		}
		function doCopy(mail, comment) {
			if (is_ie()) {
				window.clipboardData.setData("Text", mail);
				alert(comment + " Email 주소가 클립보드에\n복사되었습니다.\n붙여넣기 하여 사용해주세요.");
			} else {
				prompt(comment + " Email 주소입니다. Ctrl+C를 눌러 복사하세요.", mail);
			}
		}
	</script>
</body>
</html>