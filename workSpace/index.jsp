<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title>index</title>
		<meta name="description" content="">
		<meta name="author" content="site.kim">

		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
		<link rel="shortcut icon" href="/favicon.ico">
		<link rel="apple-touch-icon" href="/apple-touch-icon.png">
	
		<link rel="stylesheet" href="/css/bootstrap.css" type="text/css" media="screen" title="notitle" charset="UTF-8" />
		<link rel="stylesheet" href="/css/font-awesome.css" type="text/css" media="screen" title="notitle" charset="UTF-8" />
		<link rel="stylesheet" href="/css/style.css" type="text/css" media="screen" title="notitle" charset="UTF-8" />
		<!--[if IE 7]>
			<link rel="stylesheet" href="/css/font-awesome-ie7.min.css">
		<![endif]-->
		<!--[if lt IE 9]>
			<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="/js/bootstrap.js" ></script>
	</head>	
	<body>
		<div>
			<header>
				<h1>안녕</h1>
			</header>
			<nav> 
				<p>
					<a href="/">Home</a>
				</p>
				<p>
					<a href="/contact">Contact</a>
				</p> 
			</nav> 

			<div>

			</div>

			<footer>
				<p>
					&copy; Copyright  by site.kim
				</p>
			</footer>
		</div>
	</body>
</html>
