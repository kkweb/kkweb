/* UI - 메인 */
var ui_main = {
    init: function(){

        this.pages = jQuery(".page");
        this.pageNum = this.pages.length;
        this.wh = jQuery(window).height();
        this.currentIdx = 0;
        this.pageY = [];
        this.isMoveByPage = true;

        this.animation.init();
    },

    animation: {
        init: function(){
            this.pageArr = [this.page0];
            for(var i=0; i<this.pageArr.length; i++){
                this.pageArr[i].init();
            }

            this.animate(ui_main.currentIdx, 0);

        },

        animate: function(paramPageIdx, paramDelay){
            for(var i=0; i<this.pageArr.length; i++){
                if(i == paramPageIdx) this.pageArr[i].start(paramDelay);
                else this.pageArr[i].reverse(paramDelay);
            }

        },

        page0: {
        	init: function(){
        		this.wrap = jQuery('.businessIntro');
        		var autoSet;
        		var idx = 0;
            	var leaveIdx = 0;
            	var turn = false;
            	
                this.items = this.wrap.find("a[class^=off]");
                this.pos = [0, 100, -100, 150, -100, 100, -150, 30, -100, -30, 0, -100, 0, -100, 150, -30, 100, 30, 100, 30]; // 20171214 포지션 추가
                this.items.css("opacity", 0);
                for(var i=0; i<this.pos.length/2; i++){
                    this.setPosition(this.items.eq(i), this.pos[i*2], this.pos[i*2+1]);
                }
            },
            setPosition: function(paramElement, paramX, paramY){
                paramElement.css({
                   "overwrite":1.2,
                   //"-ms-transform": "translateX("+paramX + "px, "+ paramY+"px)",
                   //"-moz-transform": "translateX("+paramX + "px, "+ paramY+"px)",
                   //"-webkit-transform": "translateX("+paramX + "px, "+ paramY+"px)",
                   "transform": "translate("+paramX + "px, "+ paramY+"px)"
               }); 
           },

           transitionPosition: function(paramElement, paramOpacity, paramTargetX, paramTargetY, paramDelay){
              TweenMax.to(paramElement, 1, {
                   "delay":paramDelay,
                   "overwrite":1,
                   "opacity":paramOpacity,
                   //"-ms-transform": "translateX("+paramTargetX + "px, "+ paramTargetY+"px)",
                   //"-moz-transform": "translateX("+paramTargetX + "px, "+ paramTargetY+"px)",
                   //"-webkit-transform": "translateX("+paramTargetX + "px, "+ paramTargetY+"px)",
                   "transform": "translate("+paramTargetX + "px, "+ paramTargetY+"px)",
                   ease:Back.easeOut
               }); 
            },
            start: function(paramDelay){
            	if(this.active) return;
				this.active = true;
				//var page0 = this;

                for(var i=0; i<this.pos.length/2; i++){
                    this.transitionPosition(this.items.eq(i), 1, 0, 0, paramDelay + i * 0.1);
                }; 

				idx = 0;
            	leaveIdx = 0;
            	var length = $('.tv_channel li').size();
            	
            	//초기화
            	$('.tv_channel a.on, .tv_channel .ch_num').hide();
            	
            	if( this.turn == true){
            		setTimeout(function(){
            			autoSet = setInterval(function(){
                    		if(idx >= length) idx = 0;
                    		$('.tv_channel li').find('.on').stop().fadeOut('1000','easeInQuad');
                    		$('.tv_channel li').find('.ch_num').stop().fadeOut('500','easeInQuad');
                    		$('.tv_channel li').eq(idx).find('.on').stop().fadeIn('1000','easeInQuad');
                    		$('.tv_channel li').eq(idx).find('.ch_num').stop().fadeIn('500','easeInQuad');
                    		idx++;
        				}, 1700);
                	},1000);
            	}else{
            		autoSet = setInterval(function(){
                		if(idx >= length) idx = 0;
                		$('.tv_channel li').find('.on').stop().fadeOut('1000','easeInQuad');
                		$('.tv_channel li').find('.ch_num').stop().fadeOut('500','easeInQuad');
                		$('.tv_channel li').eq(idx).find('.on').stop().fadeIn('1000','easeInQuad');
                		$('.tv_channel li').eq(idx).find('.ch_num').stop().fadeIn('500','easeInQuad');
                		idx++;
    				}, 1700);
            	}
            	
            	$('.tv_channel li').mouseenter(function(){
            		clearInterval(autoSet);
            		$('.tv_channel li').find('.on').stop().fadeOut('1000','easeInQuad');
            		$('.tv_channel li').find('.ch_num').stop().fadeOut('500','easeInQuad');
            		$(this).find('.on').stop().fadeIn('1000','easeInQuad');
            		$(this).find('.ch_num').stop().fadeIn('500','easeInQuad');
            	});
            	
            	$('.tv_channel li').mouseleave(function(){
            		idx = $(this).index();
            	});
            	
            	$('.tv_channel > ul').mouseleave(function(){
            		clearInterval(autoSet);
            		autoSet = setInterval(function(){
                		if(idx >= length) idx = 0;
                		$('.tv_channel li').find('.on').stop().fadeOut('1000','easeInQuad');
                		$('.tv_channel li').find('.ch_num').stop().fadeOut('500','easeInQuad');
                		$('.tv_channel li').eq(idx).find('.on').stop().fadeIn('1000','easeInQuad');
                		$('.tv_channel li').eq(idx).find('.ch_num').stop().fadeIn('500','easeInQuad');
                		idx++;
    				}, 1700);
            	});
            },

            reverse: function(paramDelay){
                if(this.active) {
                    for (var i = 0; i < this.pos.length / 2; i++) {
                        this.transitionPosition(this.items.eq(i), 0, this.pos[i * 2], this.pos[i * 2 + 1], paramDelay);
                    };
                    clearInterval(autoSet);
                    this.active = false;
                    this.turn = true;
                } 
            }
        },
        
    }
};
ui_main.init();