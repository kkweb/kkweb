$(function(){

	//GNB
	$('.gnb ul li a').mouseover(function(){
		$('.snb').slideDown();
	});			
			
	$('header').mouseleave(function(){
		$('.snb').slideUp();				
	});

	// selectbox
	$(".select_box > ul").on("click", ".init", function() {
		$(this).closest(".select_box > ul").children('li:not(.init)').slideDown(100);
	});

	var allOptions = $(".select_box > ul").children('li:not(.init)');
	$(".select_box > ul").on("click", "li:not(.init)", function() {
		allOptions.removeClass('selected');
		$(this).addClass('selected');
		$(".ico_select").addClass('active');
		$(".select_box > ul").children('.init').html($(this).html());
		allOptions.slideUp(100);
	});
	
	$(".ico_select").addClass('active');
	$(".ico_select").on('click', function(){
		if($(this).hasClass('active')){
			$(this).removeClass('active');
			$(".select_box > ul").children('li:not(.init)').slideDown(100);
		}else{
			$(this).addClass('active');
			$(".select_box > ul").children('li:not(.init)').slideUp(100);
		}
	});

	// page navigation
	$(".route_list").addClass('active');
	$(".route_title > a").on('click', function(){
		if($(".route_list").hasClass('active')){
			$(".route_list").removeClass('active');
			$(".route_list").slideDown(100);
		}else{
			$(".route_list").addClass('active');
			$(".route_list").slideUp(100);
		}
	});
	// page navigation
	$(".route_list02").addClass('active');
	$(".route_title02 > a").on('click', function(){
		if($(".route_list02").hasClass('active')){
			$(".route_list02").removeClass('active');
			$(".route_list02").slideDown(100);
		}else{
			$(".route_list02").addClass('active');
			$(".route_list02").slideUp(100);
		}
	});
	// FAQ 
	$('.faqContent dt a').on('click', function(){
		var faqElement = $(this).parents('tr');
		if(faqElement.hasClass('open')){
			faqElement.removeClass('open');
			faqElement.find('dd').removeClass('open');
			faqElement.find('.answerContent').removeClass('open');
			faqElement.find('.answerContent').slideUp(100);
		}else{			
			faqElement.addClass('open');
			faqElement.find('dd').addClass('open');
			faqElement.find('.answerContent').slideDown(100);
			faqElement.siblings('tr').find('.answerContent').slideUp(100);
			faqElement.siblings('tr').removeClass('open');
			faqElement.siblings('tr').find('dd').removeClass('open');
		}
	});

	//tab('#tab', 0);
	tab('#faqTab', 0);
	tab('#ethicsTab', 0);
	tab('#businessTab', 0);

	function tab2(e, num){
		var menu2 = $(e).children();
		var con2 = $(e+'_con').children();
		var select2 = $(menu2).eq(num);
		var i = num;

		select2.addClass('on');
		con2.eq(num).show();

		menu2.hover(function(){
			if(select2!==null){
				select2.removeClass("on");
				con2.eq(i).hide();
			}

			select2 = $(this);	
			i = $(this).index();

			select2.addClass('on');
			con2.eq(i).show();
		});
	}

	tab2('#productTab', 0);

	// terms selector 
	$('.termsBox > .list li').on('click', function(){
		var termOn = $(this);
		if(termOn.hasClass('colorB')){
			termOn.removeClass('colorB');
		}else{
			$('.termsBox > .list li').removeClass('colorB');
			termOn.addClass('colorB');
		}
	});

});

//Tab menu
function tab(e, num){
	var menu = $(e).children();
	var con = $(e+'_con').children();
	var select = $(menu).eq(num);
	var i = num;

	select.addClass('on');
	con.eq(num).show();

	menu.click(function(){
		if(select!==null){
			select.removeClass("on");
			con.eq(i).hide();
		}

		select = $(this);	
		i = $(this).index();

		select.addClass('on');
		con.eq(i).show();
	});
}

//slideBanner
$.fn.slideBanner=function(options){
	var opts= jQuery.extend({},jQuery.fn.slideBanner.defaults,options);
	return this.each(function(){
		var obj=this;
		var $slideWrap=$(obj);

		if($slideWrap.find('.ui-slide-mask').length<1){
			var speed=opts.speed;
			var $slideList=$slideWrap.find('.'+opts.listClass);
			var $slideItem=$slideWrap.find('.'+opts.itemClass);
			var viewItem=opts.viewItem;
			var itemWidth=$slideWrap.width();
			$slideItem.css('width',itemWidth);
			var firstItem=$slideItem.eq(0).clone().addClass('clone');
			var lastItem=$slideItem.eq(-1).clone().addClass('clone');
			$slideItem.eq(0).before(lastItem);
			$slideItem.eq(-1).after(firstItem);
			var itemLength=$slideWrap.find('.'+opts.itemClass).length;
			var listWidth=itemWidth*itemLength;
			$slideList.css({'width':listWidth,'position':'relative','left':0});
			$slideList.wrap('<div class="ui-slide-mask" style="overflow:hidden;"></div>');
			var shortcut=$('<div class="'+opts.shortcutClass+'"></div>').appendTo($slideWrap);
			var $shortcut=$slideWrap.find('.'+opts.shortcutClass);
			var btnGroup=$('<div class="ui-slide-btn-group"><button class="'+opts.prevButtonClass+'"  >이전</button><button class="'+opts.nextButtonClass+'">다음</buttona></div>').appendTo($slideWrap);

			var autoplayBox=$('<div class="'+opts.autoplayClass+'"><button class="ui-slide-play"><span>재생<span></button> <button class="ui-slide-pause"><span>정지</span></button></div>').appendTo($slideWrap);
			var $autoplayBox=$slideWrap.find('.'+opts.autoplayClass);
			var playBtn=$autoplayBox.find('.ui-slide-play');
			var pauseBtn=$autoplayBox.find('.ui-slide-pause');
			var prevBtn=$slideWrap.find('.'+opts.prevButtonClass);
			var nextBtn=$slideWrap.find('.'+opts.nextButtonClass);
			if(!opts.repeat){prevBtn.addClass('off')}
			$(window).resize(function(){
				if(opts.resize){
					itemWidth=$slideWrap.width();			
					listWidth=itemWidth*itemLength;
					firstItem.width(itemWidth);
					lastItem.width(itemWidth);
					$slideItem.css('width',itemWidth);
					$slideList.css({'width':listWidth});
					slideMove($shortcut.find('.'+opts.activeClass).index()+1);
				}
			});

			var bnIdx=1;
			var bnLen=itemLength-2;
			$slideList.css({"left": -itemWidth});

			//숏컷생성
			var sNum=0;
			for (i=1;i<=bnLen ;i++ ){
				if(i<=bnLen){
					sNum=i;
					var sc='<button><span>'+sNum+'</span></button>'
					shortcut.append(sc);
				}
				$shortcut.children().eq(0).addClass(opts.activeClass);
			}
			//숏컷이동
			$shortcut.children().each(function(){
				$(this).click(function(){
					//alert('aa')
					var nIndex = $(this).index()+1
					slideMove(nIndex);
					return false;
				});
			});
			//숏컷 활성화
			function scActive(idx){
				$shortcut.children().removeClass(opts.activeClass).eq(idx).addClass(opts.activeClass);			
			}
			//다음버튼 클릭이벤트
			nextBtn.click(function(e){
				var nIndex = bnIdx+1;
				slideMove(nIndex);
				return false;
			});

			//이전버튼 클릭이벤트
			prevBtn.click(function(e){
				var nIndex = bnIdx-1;
				slideMove(nIndex);
				return false;
			});

			function slideMove(nIndex,e){
				if (nIndex != bnIdx) {
					var xPos = -itemWidth * nIndex;
					$slideList.stop().animate({
						left: xPos
					}, speed, function(){
						if(!opts.repeat){
							nextBtn.removeAttr('disabled').removeClass('off');
							prevBtn.removeAttr('disabled').removeClass('off');
						};
						if(nIndex<1) {
							nIndex = bnLen;
							xPos = -itemWidth * nIndex;
							$slideList.css({"left": xPos + "px"});

						}else if(nIndex<2&&!opts.repeat){
							prevBtn.attr('disabled','disabled').addClass('off');
						}
						if(nIndex>bnLen) {
							nIndex = 1;
							xPos = -itemWidth * nIndex;
							$slideList.css({"left": xPos + "px"});
						}else if(nIndex>bnLen-1&&!opts.repeat){
							nextBtn.attr('disabled','disabled').addClass('off');
						}
						bnIdx = nIndex;
						scActive(bnIdx-1)
					});
				}else{
					var xPos = -itemWidth * nIndex;
					$slideList.stop().animate({left: xPos}, speed)
				}		
			};


			if(opts.repeat){
				//자동 이동 관련
				var autoSlide;
				//자동실행 클릭
				playBtn.click(function(){
					if(!$(this).hasClass(opts.activeClass)){
						pauseBtn.removeClass(opts.activeClass);
						$(this).addClass(opts.activeClass);
						autoSlide=setInterval(function(){
							var nIndex = bnIdx+1;
							slideMove(nIndex);
						},opts.delay);
					}
					return false;
				});
				//일시정지 클릭
				pauseBtn.click(function(){
					playBtn.removeClass('on');
					$(this).addClass('on');
					clearInterval(autoSlide);
				})

				//슬라이드공간에 마우스 들어올시 자동실행 멈춤
				$slideWrap.find('a').mouseenter(function(){
					if(playBtn.hasClass(opts.activeClass)){
						clearInterval(autoSlide);
					}
				});
				$slideWrap.find('button').mouseenter(function(){
					if(playBtn.hasClass(opts.activeClass)){
						clearInterval(autoSlide);
					}
				});
				//슬라이드 공간에서 마우스 나갈시 자동실행 실행
				$slideWrap.find('a').bind('mouseleave mouseup',function(){
					if(playBtn.hasClass(opts.activeClass)){
						clearInterval(autoSlide);
						autoSlide=setInterval(function(){
							var nIndex = bnIdx+1;
							slideMove(nIndex);
						},opts.delay);
					}
				});
				$slideWrap.find('button').bind('mouseleave mouseup',function(){
					if(playBtn.hasClass(opts.activeClass)){
						clearInterval(autoSlide);
						autoSlide=setInterval(function(){
							var nIndex = bnIdx+1;
							slideMove(nIndex);
						},opts.delay);
					}
				});

				if(!opts.controllerShow){$autoplayBox.hide();}
				if(opts.autoplay){playBtn.trigger('click');}else{pauseBtn.trigger('click');}
			}else{
				$autoplayBox.hide();
			}

			if(!opts.shortcutShow){$shortcut.hide();}
			if(!opts.buttonShow){$slideWrap.find('.ui-slide-btn-group').hide();}
		}
		$.fn.slideDestroy=function(options){
			var opts= jQuery.extend({},$.fn.slideBanner.defaults,options);
			return this.each(function(){
				if($slideWrap.find('.ui-slide-mask').length>0){
					$(this).find('.clone').remove();
					$(this).find('.'+opts.listClass).unwrap().css({'left':0});
					$(this).find('.'+opts.autoplayClass).remove()
					$(this).find('.'+opts.shortcutClass).remove();
					$(this).find('.ui-slide-btn-group').remove();
				}
			});
		}
	});
};


$.fn.slideBanner.defaults={
	viewItem:1,//
	resize:true,
	repeat:true,//
	controllerShow:true,
	shortcutShow:true,
	buttonShow:true,
	speed:600,
	delay:5000,
	autoplay:true,
	autoplayClass:'ui-slide-controller',
	shortcutClass:'ui-slide-shortcut',
	prevButtonClass:'ui-slide-prev-btn',
	nextButtonClass:'ui-slide-next-btn',
	listClass:'ui-slide-banner-list',
	itemClass:'ui-slide-banner-list-item',
	activeClass:'on'
};
$(function(){
	$('#slideBanner1').slideBanner();
});

/*
;(function( $, window, document, undefined ){

	var BackgroundTransition = function( elem, options ){
		this.elem = elem;
		this.$elem = $(elem);
		this.options = options;
		self = this;
	};

	BackgroundTransition.prototype = {
		defaults: {
			classNameBottomImage: "image-bottom",
			classNameTopImage: "image-top",
			idNameDownloadImage: "image-download",
			backgrounds: [],
			imageKey: 1,
			transitionDelay: 10,
			animationSpeed: 1000
		},

		init: function() {
			this.config = $.extend({}, this.defaults, this.options);
			if (this.config.backgrounds.length >= 2) {
				this.prepareMarkup();
				this.loadNext();
				return this;
			} else {
				console.warn('BackgroundTransition requires at least 2 background images.')
				return false;
			}
		},

		prepareMarkup: function() {
			var imageBottom = $("<div/>").addClass(this.config.classNameBottomImage + ' initial').css('background-image', 'url(' + this.config.backgrounds[0].src + ')');
			var imageTop = $("<div/>").addClass(this.config.classNameTopImage).css('display', 'none');
			$(this.elem)
				.prepend(imageBottom, imageTop)
				.css('background-image', 'none');
		},

		loadNext: function() {
			if (this.config.imageKey == this.config.backgrounds.length){
				this.config.imageKey = 0;
			}
			var deferred = $.Deferred();
			$('<img/>').attr('id', this.config.idNameDownloadImage).load(function() {
				deferred.resolve();
			}).attr('src', this.config.backgrounds[this.config.imageKey].src).prependTo('body .backgroundTransition');
			deferred.done(function() {
				setTimeout(self.replaceImage, (self.config.transitionDelay * 1000));
			});
		},

		replaceImage: function() {
			var nextSrc = $('#' + self.config.idNameDownloadImage);
			$('#' + self.config.idNameDownloadImage).remove();
			$('.' + self.config.classNameTopImage).css('background-image', 'url(' + nextSrc.attr('src') + ')');
			$('.' + self.config.classNameTopImage).fadeIn(self.config.animationSpeed, 'swing', function() {
				$('.' + self.config.classNameBottomImage).css('background-image', 'url(' + nextSrc.attr('src') + ')');
				$(this).hide();
				self.config.imageKey++;
				self.loadNext();
			});
		}
	}

	BackgroundTransition.defaults = BackgroundTransition.prototype.defaults;

	$.fn.backgroundTransition = function(options) {
		return this.each(function() {
			new BackgroundTransition(this, options).init();
		});
	};

})( jQuery, window , document );*/